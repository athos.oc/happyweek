import { IUsuarioService } from '../../core/usuario/IUsuarioService';
import Usuario from '../../core/usuario/Usuario';

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////// Esto es un mock /////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

export default class UsuarioService implements IUsuarioService {
  public async isUserRegistered(usuario: Usuario): Promise<boolean> {
    return true;
  }
  public async register(usuario: Usuario): Promise<boolean> {
    return true;
  }

  public async login(usuario: Usuario): Promise<boolean> {
    if ((usuario.getNombre() === 'athos' && usuario.getPass() === '123456') || 1 === 1) {
      return true;
    }
    return false;
  }

  public async sendNotification(usuario: Usuario): Promise<boolean> {
    return true;
  }

  public async saveDayHappyLevel(state: string, numberOfWeek: number): Promise<boolean> {
    return true;
  }

  public getHappyStateOfWeek(numberOfWeek: any): Promise<string> {
    return undefined;
  }
}
