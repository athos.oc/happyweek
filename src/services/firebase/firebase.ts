import * as firebase from 'firebase';

export class Firebase {
  constructor() {
    try {
      firebase.initializeApp({
        apiKey: 'AIzaSyCBnciqx7VHZwJg6F_BDx1sFqumcfqf3BY',
        authDomain: 'happyweek-d48fe.firebaseapp.com',
        databaseURL: 'https://happyweek-d48fe.firebaseio.com',
        messagingSenderId: '626340286063',
        projectId: 'happyweek-d48fe',
        storageBucket: '',
      });
    } catch (err) {
      // we skip the "already exists" message which is
      // not an actual error when we're hot-reloading
      if (!/already exists/.test(err.message)) {
        throw new Error('Firebase initialization error' + err.stack);
      }
    }
  }
  public firestore() {
    return firebase.firestore();
  }
}
