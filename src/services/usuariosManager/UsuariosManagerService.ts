import faker = require('faker');
import Usuario from '../../core/usuario/Usuario';
import { IUsuariosManagerService } from '../../core/usuariosManager/IUsuariosManagerService';
import UsuarioService from '../usuario/UsuarioService';

export default class UsuariosManagerService implements IUsuariosManagerService {
  public getUsuarios(): Usuario[] {
    const usuarioService = new UsuarioService();
    const usuariosList: Usuario[] = [];

    for (let i = 0; i < 100; i++) {
      const usuario = new Usuario(usuarioService);
      usuario.setNombre(faker.name.findName());
      usuario.setEmail(faker.internet.email());
      usuario.setPass(faker.internet.password());

      usuariosList.push(usuario);
    }
    return usuariosList;
  }
}
