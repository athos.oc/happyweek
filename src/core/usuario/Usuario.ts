import UsuarioService from '../../services/usuario/UsuarioService';
import Global from '../global/Global';
import HappyState from '../happyState/Happystate';
import { happyStates } from '../happyStates/happyStates';
import { IUsuarioService } from './IUsuarioService';

export default class Usuario implements IUsuarioService {
  public service: UsuarioService;
  private nombre: string = '';
  private email: string = '';
  private pass: string = '';
  private happyStates: HappyState[] = [];

  constructor(service: UsuarioService) {
    this.service = service;
  }

  public setNombre(nombre: string): void {
    this.nombre = nombre;
  }

  public setEmail(email: string): void {
    this.email = email;
  }

  public setPass(pass: string): void {
    this.pass = pass;
  }

  public getNombre(): string {
    return this.nombre;
  }

  public getEmail(): string {
    return this.email;
  }

  public getPass(): string {
    return this.pass;
  }

  public async login(): Promise<boolean> {
    return this.service.login(this).then(result => {
      if (result === true) {
        Global.getInstance().activeUser = this;
        return true;
      } else {
        return false;
      }
    });
  }

  public register(): Promise<boolean> {
    return this.service.register(this);
  }

  public sendNotification(): Promise<boolean> {
    return this.service.sendNotification(this);
  }

  public saveDayHappyLevel(state: string, numberOfWeek: number): Promise<boolean> {
    if (happyStates.indexOf(state) === -1) {
      return Promise.resolve(false);
    } else {
      return this.service.saveDayHappyLevel(state, numberOfWeek);
    }
  }

  public getHappyStateOfWeek(numberOfWeek: number): Promise<string> {
    return this.service.getHappyStateOfWeek(numberOfWeek);
  }

  public isUserRegistered(): Promise<boolean> {
    return this.service.isUserRegistered(this);
  }
}
