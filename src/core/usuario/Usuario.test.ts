import UsuarioService from '../../services/usuario/UsuarioService';
import Usuario from '../usuario/Usuario';

let usuario: Usuario;

beforeEach(() => {
  const service = new UsuarioService();
  usuario = new Usuario(service);
  usuario.setNombre('athos');
  usuario.setEmail('athos.oc@gmail.com');
  usuario.setPass('123456');
});

test('getNombre', () => {
  expect(usuario.getNombre()).toEqual('athos');
});

test('getNombre', () => {
  expect(usuario.getEmail()).toEqual('athos.oc@gmail.com');
});

test('getNombre', () => {
  expect(usuario.getPass()).toEqual('123456');
});

test('login correcto', async () => {
  const isCorrect = await usuario.login();
  expect(isCorrect).toBe(true);
});

xtest('login INcorrecto', async () => {
  usuario.setPass('fds');
  const isCorrect = await usuario.login();
  expect(isCorrect).toBe(false);
});

test('sendNotification', async () => {
  spyOn(usuario.service, 'sendNotification');
  usuario.sendNotification();
  expect(usuario.service.sendNotification).toHaveBeenCalled();
});

test('saveDayHappyLevel con un valor no valido', async () => {
  const resultado = await usuario.saveDayHappyLevel('fdsfds', 32);
  expect(resultado).toBe(false);
});

test('saveDayHappyLevel con un valor valido', async () => {
  const resultado = await usuario.saveDayHappyLevel('Sobresaliente', 32);
  expect(resultado).toBe(true);
});

xtest('getHappyStateOfThisWeek', async () => {
  const resultado = await usuario.getHappyStateOfWeek(1);
  expect(typeof resultado).toEqual('string');
});
