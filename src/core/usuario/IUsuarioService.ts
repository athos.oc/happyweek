import Usuario from './Usuario';

export interface IUsuarioService {
  isUserRegistered(usuario: Usuario): Promise<boolean>;
  register(usuario: Usuario): Promise<boolean>;
  login(usuario: Usuario): Promise<boolean>;
  sendNotification(usuario: Usuario): Promise<boolean>;
  saveDayHappyLevel(state: string, numberOfWeek: number): Promise<boolean>;
  getHappyStateOfWeek(numberOfWeek: number): Promise<string>;
}
