import Global from './Global';

test('global singleton', () => {
  const global1: Global = Global.getInstance();
  const global2: Global = Global.getInstance();
  expect(global1).toBe(global2);
});
