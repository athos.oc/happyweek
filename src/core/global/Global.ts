import Usuario from '../usuario/Usuario';

export default class Global {
  public static getInstance() {
    if (this.instance === null) {
      this.instance = new Global();
    }
    return this.instance;
  }
  private static instance: Global = null;

  public activeUser: Usuario;
  private constructor() {}
}
