import UsuariosManagerService from '../../services/usuariosManager/UsuariosManagerService';
import Usuario from '../usuario/Usuario';
import UsuariosManager from './UsuariosManager';

let usuariosManager: UsuariosManager;

beforeEach(() => {
  const service = new UsuariosManagerService();
  usuariosManager = new UsuariosManager(service);
});

test('getUsuarios devolvera objetos del tipo Usuario', () => {
  const usuarios: Usuario[] = usuariosManager.getUsuarios();
  expect(usuarios[0] instanceof Usuario).toBe(true);
});

test('getUsuarios devolvera un Array', () => {
  const usuarios: Usuario[] = usuariosManager.getUsuarios();
  expect(usuarios instanceof Array).toBe(true);
});
