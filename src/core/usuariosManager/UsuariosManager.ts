import UsuariosManagerService from '../../services/usuariosManager/UsuariosManagerService';
import Usuario from '../usuario/Usuario';
import { IUsuariosManagerService } from './IUsuariosManagerService';

export default class UsuariosManager implements IUsuariosManagerService {
  public usuariosList: Usuario[];
  private service: UsuariosManagerService;

  constructor(service: UsuariosManagerService) {
    this.service = service;
  }

  public getUsuarios(): Usuario[] {
    return this.service.getUsuarios();
  }
}
