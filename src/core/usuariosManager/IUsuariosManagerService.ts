import Usuario from '../usuario/Usuario';

export interface IUsuariosManagerService {
  getUsuarios(): Usuario[];
}
