import { happyStates } from '../happyStates/HappyStates';
export default class Helper {
  public static getHappyStatesOptions(): string[] {
    return happyStates;
  }
}
