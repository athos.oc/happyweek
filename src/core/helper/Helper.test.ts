import Helper from './Helper';

test('getHappyStatesOptions', () => {
  expect(Helper.getHappyStatesOptions()).toEqual([
    'Sobresaliente',
    'Notable',
    'Bien',
    'Suficiente',
    'Insuficiente',
    'MuyDeficiente',
  ]);
});
